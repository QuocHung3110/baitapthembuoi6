// start BT1
function primeNumber(n) {
  var count = 0;
  for (var i = 2; i <= n / 2; i++) {
    if (n == 2 || n == 3) {
      return true;
    }
    if (count == 1) {
      return false;
    } else {
      if (n % i == 0) {
        count++;
      }
    }
  }
  return true;
}

function listPrimeNumber() {
  var n3 = document.getElementById("n3").value * 1;
  if (n3 < 0) {
    return alert("Vui lòng nhập lại số n >= 0");
  }

  var result = "";
  for (var i = 2; i <= n3; i++) {
    if (primeNumber(i) == true) {
      result = result + " " + i;
    }
  }
  console.log(result);
  document.getElementById("listPrimeNumber").innerHTML = `${result}`;
}
// end BT1

// start BT2
function findOddEven() {
  var odd = "Số chẵn:";
  var even = "Số lẻ:";
  for (var i = 1; i < 100; i++) {
    if (i % 2 == 0) {
      odd = odd + ` ` + i;
    } else {
      even = even + " " + i;
    }
  }
  document.getElementById(
    "findOddEven"
  ).innerHTML = `<b style="color:#C20000">${odd}</b>
                                                    <br/><b style="color:#002795">${even}</b>`;
}
// end BT2

// start BT3
function divisibleBy3() {
  var count = 0;
  for (var i = 0; i <= 1000; i++) {
    if (i % 3 == 0) {
      count++;
    }
  }
  document.getElementById(
    "divisibleBy3"
  ).innerHTML = ` Số chia hết cho 3 nhỏ hơn 1000: &nbsp <b>${count} số</b>`;
}
// end BT3
